# cfgmgmt101

## Ansible

Installation is trivial using homebrew on OS X.
Make sure you have Xcode installed.

```bash
xcode-select --install
/usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
brew update
brew install ansible
```

